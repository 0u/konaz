# konaz

Hi! I'm a simple chess engine that Uli is developing for fun, you can play me [here](https://lichess.org/@/konaz)

# TODO
* MTD-bi ala sunfish
* Transposition table
* More testcases

# Bugs
* Uses all its time even when it has one legal move.
* Tries to go from depth 4 -> 5 with no hope of finishing a deeper search in allocated time.
* Does not respond to engine `stop` command while searching.

# Credits

[UlisseMini](https://github.com/UlisseMini) Author and maintainter
