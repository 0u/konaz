// evaluation function for a chess position and move.
use chess;
use chess::BoardStatus::*;
use chess::Color;
use chess::Piece;

pub const MATE: i32 = 10000;

/// Our evaluation is in centipawns.
/// pawn = 100
/// knight = 300
/// etc.
pub fn eval(board: &chess::Board) -> i32 {
    match board.status() {
        Checkmate => {
            if board.side_to_move() == Color::Black {
                MATE
            } else {
                -MATE
            }
        }
        Stalemate => 0,
        Ongoing => value_ongoing(&board),
    }
}

#[inline]
fn eval_move(board: &chess::Board, mv: chess::ChessMove) -> i32 {
    eval(&board.make_move_new(mv))
}

// helper for value_ongoing
#[inline]
fn pieces(board: &chess::Board, piece: chess::Piece, value: i32) -> i32 {
    let black_pieces = (board.pieces(piece) & board.color_combined(Color::Black)).popcnt();
    let white_pieces = (board.pieces(piece) & board.color_combined(Color::White)).popcnt();

    return ((white_pieces as i32) - (black_pieces as i32)) * value;
}

#[inline]
fn value_pieces(board: &chess::Board) -> i32 {
    0 + pieces(board, Piece::Queen, 900)
        + pieces(board, Piece::Rook, 500)
        + pieces(board, Piece::Knight, 300)
        + pieces(board, Piece::Bishop, 300)
        + pieces(board, Piece::Pawn, 100)
}

#[inline]
fn value_ongoing(board: &chess::Board) -> i32 {
    // no idea why it fails to cap free knight when i don't /100 this.
    value_pieces(board) + (chess::MoveGen::new_legal(board).count() as i32) / 100
}

#[cfg(test)]
mod tests {
    use super::*;
    use chess::{ChessMove, Square};
    use std::str::FromStr;

    // NOTE: Tests only cover value_pieces, since its hard to test when legal moves is a variable

    fn test_eval(fen: &str, want: i32) {
        assert_eq!(
            value_pieces(&chess::Board::from_str(fen).expect("Valid fen")),
            want
        );
    }

    #[test]
    fn test_eval_startpos() {
        test_eval(
            "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1",
            0,
        );
    }

    #[test]
    fn test_eval_white_pawn_up() {
        test_eval(
            "rnbqkbnr/pppp1ppp/8/4N3/8/8/PPPPPPPP/RNBQKB1R b KQkq - 0 2",
            100,
        );
    }
}
