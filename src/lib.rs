use chess::{Action, ChessMove, Color, MoveGen};
use std::cmp::{max, min};
use std::io;
use std::str::FromStr;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;
use std::thread;
use std::time::Duration;
use stoplight;

use std::ops::Neg;

#[cfg(test)]
mod move_tests;

mod eval;
mod uci;

// Our engine.
pub struct Engine {
    game: chess::Game, // current game.
}

// Search result from minimax.
#[derive(Debug)]
pub struct SearchResult {
    pub eval: i32,                    // the evaluation for the position
    pub mv: Option<chess::ChessMove>, // the best move (NOTE: Maybe can remove Option now?)
    pub depth: u16,                   // the depth past this node the position was searched.
}

impl Neg for SearchResult {
    type Output = SearchResult;

    fn neg(self) -> Self::Output {
        SearchResult {
            eval: -self.eval,
            mv: self.mv,
            depth: self.depth,
        }
    }
}

fn game_over(board: &chess::Board) -> bool {
    use chess::BoardStatus::*;
    match board.status() {
        Checkmate => true,
        Stalemate => true,
        Ongoing => false,
    }
}

fn ab_white(mut alpha: &mut i32, beta: &mut i32, eval: i32) {
    if eval > *alpha {
        *alpha = eval
    }
}

fn ab_black(alpha: &mut i32, mut beta: &mut i32, eval: i32) {
    if eval < *beta {
        *beta = eval;
    }
}

pub fn alphabeta(
    board: &chess::Board,
    depth: u16,
    mut alpha: i32,
    mut beta: i32,
    stop: Arc<AtomicBool>,
) -> Option<SearchResult> {
    if stop.load(Ordering::Relaxed) {
        // We must invalidate this tree since we have not fully explored it.
        return None;
    }

    if depth == 0 || game_over(board) {
        // Return static evaluation
        return Some(SearchResult {
            eval: eval::eval(board),
            mv: None,
            depth: depth,
        });
    }

    let white = board.side_to_move() == Color::White;
    let mut best: Option<SearchResult> = None;

    let better = if white { i32::gt } else { i32::lt };

    let ab: fn(&mut i32, &mut i32, i32);
    if white {
        ab = ab_white;
    } else {
        ab = ab_black;
    }

    for mv in MoveGen::new_legal(board) {
        let mut result = alphabeta(
            &board.make_move_new(mv),
            depth - 1,
            alpha,
            beta,
            stop.clone(),
        )?;

        result.mv = Some(mv);

        // alpha = max(alpha, result.eval);
        ab(&mut alpha, &mut beta, result.eval);

        // best = best.or(Some(result));
        best = Some(match best.as_mut() {
            Some(mut b) => {
                if better(&result.eval, &b.eval) {
                    result
                } else {
                    best.unwrap()
                }
            }

            None => result,
        });

        if beta <= alpha {
            break; // prune
        }
    }

    // Make sure we do the shortest possible checkmate.
    if let Some(b) = best.as_mut() {
        if i32::abs(b.eval) == eval::MATE {
            if white {
                b.eval -= b.depth as i32;
            } else {
                b.eval += b.depth as i32;
            }
        }
    }

    return best;
}

pub fn minimax(board: &chess::Board, depth: u16, stop: Arc<AtomicBool>) -> SearchResult {
    let color = board.side_to_move() == Color::White;
    if depth == 0 || game_over(board) {
        return SearchResult {
            eval: eval::eval(board),
            mv: None,
            depth: depth,
        };
    }

    let mut best = SearchResult {
        eval: if color { -eval::MATE } else { eval::MATE },
        mv: None,
        depth: depth - 1,
    };

    // let better = if color { i32::gt } else { i32::lt };

    if color {
        for mv in MoveGen::new_legal(board) {
            let result = minimax(&board.make_move_new(mv), depth - 1, stop.clone());
            if result.eval > best.eval {
                best = result;
                best.depth = depth;
                best.mv = Some(mv);
            }
        }
    } else {
        for mv in MoveGen::new_legal(board) {
            let result = minimax(&board.make_move_new(mv), depth - 1, stop.clone());
            if result.eval < best.eval {
                best = result;
                best.mv = Some(mv);
            }
        }
    }

    // for mv in MoveGen::new_legal(board) {
    //     let result = minimax(&board.make_move_new(mv), depth - 1, stop.clone());
    //     if better(&result.eval, &best.eval) {
    //         best = result;
    //         best.mv = Some(mv);
    //     }
    // }

    best
}

// TODO: Add alphabeta
// TODO: Add move sorting
// TODO: Add memory to minimax
// TODO: Add quietness search
// NOTE: Very importent to override past best_eval with deepest for accuracy.
// FIXME: Race condition where if we are not given enough time we will not return a move.
fn iddfs(board: &chess::Board, stop: Arc<AtomicBool>) -> Option<SearchResult> {
    let white = board.side_to_move() == Color::White;
    let mut best: Option<SearchResult> = None;
    let better = if white { i32::gt } else { i32::lt };

    for depth in 1..1000 {
        // best = minimax(board, depth, stop.clone());
        let result = alphabeta(board, depth, std::i32::MIN, std::i32::MAX, stop.clone());
        match best.as_mut() {
            Some(mut b) => {
                if let Some(res) = result {
                    *b = res;
                }
                // println!("info depth {} score cp {}", depth, b.eval);
            }
            None => {
                best = result; // anything is better then nothing!
            }
        }

        if stop.load(Ordering::Relaxed) {
            break;
        }
    }

    best
}

// high level search function
pub fn search(board: &chess::Board, stop: Arc<AtomicBool>) -> Option<SearchResult> {
    iddfs(board, stop)
}

// our time -> ms for move
pub fn moves_so_far(game: &chess::Game) -> u64 {
    game.actions()
        .iter()
        .filter(|a| match a {
            Action::MakeMove(_) => true,
            _ => false,
        })
        .count() as u64
}

// get the correct amount of time to sleep
fn sleep_time(game: &chess::Game, go: uci::Go) -> Option<Duration> {
    use uci::TimeControl as Tc;

    if let Some(tc) = go.time_control {
        return match tc {
            Tc::MoveTime(millis) => Some(Duration::from_millis(millis)),
            Tc::TimeLeft(time) => {
                let time_left = if game.side_to_move() == Color::White {
                    time.white_time?
                } else {
                    time.black_time?
                };
                // TODO: Add support for increment

                Some(Duration::from_millis(time_left / (50 - moves_so_far(game))))
            }
            _ => None,
        };
    }
    None
}

impl Engine {
    pub fn new() -> Engine {
        Engine {
            game: chess::Game::new(),
        }
    }

    fn go(&mut self, go: uci::Go) -> ChessMove {
        let board = self.game.current_position();
        let th = stoplight::Thread::spawn(move |stop| search(&board, stop));

        thread::sleep(sleep_time(&self.game, go).unwrap_or(Duration::from_secs(1)));

        th.stop();
        th.join().unwrap().unwrap().mv.unwrap() // get best move
    }

    fn uci(&mut self) {
        println!("id name Konaz");
        println!("id author Ulisse Mini");
        println!("uciok");
    }

    fn handle(&mut self, msg: uci::Message) {
        use uci::Message;

        match msg {
            Message::Uci => self.uci(),
            Message::Position(pos) => {
                if pos.startpos {
                    self.game = chess::Game::new();
                }

                if let Some(fen) = pos.fen {
                    if let Ok(game) = chess::Game::from_str(&fen) {
                        self.game = game;
                    }
                }

                for mv in pos.moves {
                    if !self.game.make_move(mv) {
                        eprintln!("{} Is not a legal move.", mv);
                    }
                }
            }

            Message::Go(go) => {
                println!("bestmove {}", self.go(go));
            }
            Message::IsReady => println!("readyok"),
            _ => {}
        }
    }

    pub fn uci_loop(&mut self) -> io::Result<()> {
        use std::io::BufRead;

        let stdin = io::stdin();
        for line in stdin.lock().lines() {
            let line = line?;

            let msg = uci::parse(&line);

            eprintln!("Got: {}", line);
            eprintln!("Parse: {:?}", msg);
            if let Some(msg) = msg {
                self.handle(msg);
            }
        }

        Ok(())
    }
}
