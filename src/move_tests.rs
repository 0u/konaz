use crate::*;
use std::thread;
use std::time::Duration;
use stoplight::Thread;

use chess::{Board, ChessMove, Square};
use std::str::FromStr;

macro_rules! test {
    (name: $name:ident, fen: $fen:expr, want: ($sq1:ident, $sq2:ident), time: $time:expr) => {
        #[test]
        fn $name() {
            let board = Board::from_str($fen).expect("Valid Fen");

            // let th = Thread::spawn(move |stop| {
            //     alphabeta(&board, 10, std::i32::MIN, std::i32::MAX, stop.clone())
            // });
            let th = Thread::spawn(move |stop| search(&board, stop.clone()));

            thread::sleep(Duration::from_millis($time));

            th.stop();
            let result = th.join().unwrap().unwrap();

            let mv = result.mv.expect("Engine did not provide move");
            let want = ChessMove::new(Square::$sq1, Square::$sq2, None);
            if mv != want {
                panic!(
                    "want {}, got {} (eval {} depth {})",
                    want, mv, result.eval, result.depth
                );
            }
        }
    };
}

macro_rules! test_not {
    (name: $name:ident, fen: $fen:expr, dont_want: ($sq1:ident, $sq2:ident), time: $time:expr) => {
        #[test]
        fn $name() {
            let board = Board::from_str($fen).expect("Valid Fen");

            // let th = Thread::spawn(move |stop| {
            //     alphabeta(&board, 10, std::i32::MIN, std::i32::MAX, stop.clone())
            // });
            let th = Thread::spawn(move |stop| search(&board, stop.clone()));

            thread::sleep(Duration::from_millis($time));

            th.stop();
            let result = th.join().unwrap().unwrap();

            let mv = result.mv.expect("Engine did not provide move");
            let dont_want = ChessMove::new(Square::$sq1, Square::$sq2, None);
            if mv == dont_want {
                panic!(
                    "don't want {}, got {} (eval {} depth {})",
                    dont_want, mv, result.eval, result.depth,
                );
            }
        }
    };
}

test!(
    name: mate_1_white,
    fen: "r1bqkb1r/pppp1ppp/2n2n2/4p2Q/2B1P3/8/PPPP1PPP/RNB1K1NR w KQkq - 4 4",
    want: (H5, F7),
    time: 100
);

test!(
    name: mate_1_black,
    fen: "rnb1k1nr/pppp1ppp/8/2b1p3/2B1P2q/2N2N2/PPPP1PPP/R1BQK2R b KQkq - 5 4",
    want: (H4, F2),
    time: 100
);

test!(
    name: free_knight_black,
    fen: "r1bqkbnr/pppp1ppp/2n5/4N3/4P3/8/PPPP1PPP/RNBQKB1R b KQkq - 0 3",
    want: (C6, E5),
    time: 100
);

test!(
    name: free_knight_white,
    fen: "r1bqkb1r/pppp1ppp/2n5/1B2p3/4n3/2N2N2/PPPP1PPP/R1BQK2R w KQkq - 0 5",
    want: (C3, E4),
    time: 100
);

test!(
    name: lichess_mate_2,
    fen: "8/5Q1p/p2N2p1/5p1k/4p3/4P3/PP1pqPPP/5RK1 b - - 4 40",
    want: (E2, F1),
    time: 1000
);

test!(
    name: only_move,
    fen: "7r/5Q1k/6p1/3P1p1p/5P2/6P1/1P2P2P/R4RK1 b - - 1 25",
    want: (H7, H6),
    time: 100
);

test_not!(
    name: dont_take_white,
    fen: "rnbqkb1r/pppp1ppp/5n2/4p3/4P3/5Q2/PPPP1PPP/RNB1KBNR w KQkq - 2 3",
    dont_want: (F3, F6),
    time: 100
);

test_not!(
    name: dont_take_black,
    fen: "rnb1kbnr/pppp1ppp/5q2/4p3/4P3/2N2N2/PPPP1PPP/R1BQKB1R b KQkq - 7 5",
    dont_want: (F6, F3),
    time: 100
);

test!(
    name: rook_for_bishop,
    fen: "1nbqkbnr/1ppppppp/r7/p7/3PP3/8/PPP2PPP/RNBQKBNR w KQk - 1 3",
    want: (F1, A6),
    time: 100
);

test!(
    name: mate_3_fishing_pole,
    fen: "r1b1kb1r/pppp1pp1/2n5/1B2p3/4PPpq/8/PPPP2P1/RNBQNRK1 b kq f3 0 8",
    want: (G4, G3),
    time: 1000
);

test!(
    name: mate_4_fishing_pole,
    fen: "r1bqkb1r/pppp1pp1/2n5/1B2p3/4P1p1/8/PPPP1PP1/RNBQNRK1 b kq - 1 7",
    want: (D8, H4),
    time: 1000
);

test!(
    name: win_exchange_in_4, // 8 ply
    fen: "2r3k1/1p3ppp/1qnBb3/2RpPp2/3P4/rP2QN2/5PPP/1R4K1 w - - 0 1",
    want: (C5, C6),
    time: 1000
);

test!(
    name: win_rook_in_4, // 8 ply
    fen: "2br2k1/4pp1p/6pB/8/8/2q2P2/P1PrQ1PP/1R1R2K1 b - - 0 1",
    want: (C3, D4),
    time: 1000
);

test!(
    name: trap_queen_2, // 3 ply
    fen: "r1b2r1k/1p2Npbp/p2p2p1/2n5/3N1P2/4B2P/qPQ3P1/2R2RK1 w - - 0 1",
    want: (F1, A1),
    time: 1000
);

// The bot did not care about how long a mating pattern took, this test covers that.
test!(
    name: mate_1_not_2,
    fen: "r5r1/1bppkp1p/3bp3/8/4P3/2PNQ3/PK6/3q1B2 b - - 0 1",
    want: (D6, A3),
    time: 100
);
