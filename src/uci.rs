use chess::ChessMove;
use std::str::{FromStr, Split};

/// An enumeration type containing representations for all messages supported by the UCI protocol.
#[derive(Clone, PartialEq, Debug)]
pub struct Position {
    /// If `true`, it denotes the starting chess position. Generally, if this property is `true`, then the value of
    /// the `fen` property will be `None`.
    pub startpos: bool,

    /// The [FEN format](https://en.wikipedia.org/wiki/Forsyth%E2%80%93Edwards_Notation) representation of a chess
    /// position.
    pub fen: Option<String>,

    /// A list of moves to apply to the position.
    pub moves: Vec<ChessMove>,
}

impl Position {
    fn default() -> Position {
        Position {
            startpos: true,
            fen: None,
            moves: Vec::new(),
        }
    }
}

/// An enumeration type containing representations for all messages supported by the UCI protocol.
#[derive(Clone, PartialEq, Debug)]
pub enum Message {
    /// The `uci` engine-bound message.
    Uci,

    /// The `debug` engine-bound message. Its internal property specifies whether debug mode should be enabled (`true`),
    /// or disabled (`false`).
    Debug(bool),

    /// The `isready` engine-bound message.
    IsReady,

    /// The `position` engine-bound message.
    Position(Position),

    /// The `setoption` engine-bound message.
    SetOption {
        /// The name of the option to set.
        name: String,

        /// The value of the option to set. If the option has no value, this should be `None`.
        value: Option<String>,
    },

    /// The `ucinewgame` engine-bound message.
    UciNewGame,

    /// The `stop` engine-bound message.
    Stop,

    /// The `ponderhit` engine-bound message.
    PonderHit,

    /// The `quit` engine-bound message.
    Quit,

    /// The `go` engine-bound message.
    Go(Go),
}

#[derive(Clone, Eq, PartialEq, Debug)]
pub struct Go {
    /// Time-control-related `go` parameters (sub-commands).
    pub time_control: Option<TimeControl>,

    /// Search-related `go` parameters (sub-commands).
    pub search_control: Option<SearchControl>,
}

#[derive(Clone, Eq, PartialEq, Debug)]
pub struct TimeLeft {
    /// White's time on the clock, in milliseconds.
    pub white_time: Option<u64>,

    /// Black's time on the clock, in milliseconds.
    pub black_time: Option<u64>,

    /// White's increment per move, in milliseconds.
    pub white_increment: Option<u64>,

    /// Black's increment per move, in milliseconds.
    pub black_increment: Option<u64>,

    /// The number of moves to go to the next time control.
    pub moves_to_go: Option<u8>,
}

impl TimeLeft {
    fn default() -> TimeLeft {
        TimeLeft {
            white_time: None,
            black_time: None,
            white_increment: None,
            black_increment: None,
            moves_to_go: None,
        }
    }
}

/// This enum represents the possible variants of the `go` UCI message that deal with the chess game's time controls
/// and the engine's thinking time.
#[derive(Clone, Eq, PartialEq, Debug)]
pub enum TimeControl {
    /// The `go ponder` message.
    Ponder,

    /// The `go infinite` message.
    Infinite,

    /// The information about the game's time controls.
    TimeLeft(TimeLeft),

    /// Specifies how much time the engine should think about the move, in milliseconds.
    MoveTime(u64),
}

/// A struct that controls the engine's (non-time-related) search settings.
#[derive(Clone, Eq, PartialEq, Debug)]
pub struct SearchControl {
    /// Limits the search to these moves.
    pub search_moves: Vec<ChessMove>,

    /// Search for mate in this many moves.
    pub mate: Option<u8>,

    /// Search to this ply depth.
    pub depth: Option<u8>,

    /// Search no more than this many nodes (positions).
    pub nodes: Option<u64>,
}

impl SearchControl {
    fn default() -> SearchControl {
        SearchControl {
            search_moves: Vec::new(),
            mate: None,
            depth: None,
            nodes: None,
        }
    }
}

// like try but better
// runs continue on 'None'
macro_rules! pls {
    ($x:expr) => {{
        let value = $x;
        match value {
            Some(v) => v,
            None => continue,
        }
    }};
}

// TEMPORARY TILL https://github.com/jordanbray/chess/pull/27 GETS MERGED
fn move_from_string(s: String) -> Option<ChessMove> {
    use chess::{Piece, Square};

    let source = Square::from_string(s.get(0..2)?.to_string())?;
    let dest = Square::from_string(s.get(2..4)?.to_string())?;

    let mut promo = None;
    if s.len() == 5 {
        promo = Some(match s.chars().last()? {
            'q' => Piece::Queen,
            'r' => Piece::Rook,
            'n' => Piece::Knight,
            'b' => Piece::Bishop,
            _ => return None,
        });
    }

    Some(ChessMove::new(source, dest, promo))
}

// search control parse intarg
macro_rules! parse_intarg {
    ($typ:ident, $obj:ident, $field:ident, $str:expr) => {{
        let int = pls!($typ::from_str($str).ok());

        if $obj.is_none() {
            $obj = Some(SearchControl::default());
        }

        if let Some(mut x) = $obj.as_mut() {
            x.$field = Some(int);
        }
    }};
}

macro_rules! time_intarg {
    ($typ:ident, $obj:ident, $field:ident, $str:expr) => {{
        let int = pls!($typ::from_str($str).ok());
        if $obj.is_none() {
            $obj = Some(TimeControl::TimeLeft(TimeLeft::default()));
        }

        if let Some(TimeControl::TimeLeft(tl)) = $obj.as_mut() {
            tl.$field = Some(int);
        }
    }};
}

fn parse_position(mut words: Split<'_, char>) -> Option<Message> {
    let mut pos = None;

    while let Some(word) = words.next() {
        match word {
            "startpos" => {
                if pos.is_none() {
                    pos = Some(Position::default());
                }

                if let Some(pos) = pos.as_mut() {
                    pos.startpos = true;
                }
            }
            "fen" => {
                let mut fen = String::new();
                while let Some(s) = words.next() {
                    fen += s;
                }

                if pos.is_none() {
                    pos = Some(Position::default());
                }

                if let Some(pos) = pos.as_mut() {
                    pos.fen = Some(fen.to_string());
                }
            }

            "moves" => {
                if pos.is_none() {
                    pos = Some(Position::default());
                }
                let pos = pos.as_mut().unwrap(); // we set pos non None above ^^

                while let Some(word) = words.next() {
                    pos.moves.push(pls!(move_from_string(word.to_string())));
                }

                break; // not needed since we slurped all the words
            }

            _ => return None,
        }
    }

    Some(Message::Position(pos?))
}

/// Parse the 'go' uci message.
/// NOTE: If a bad 'go' command is recieved, the results are undefined
/// for example, 'go ponder infinite' might return 'infinite' as it is the last one.
/// NOTE: Even if all the args are garbage, we still want to return
/// Message::Go with no fields.
/// TODO: Use a macro / function for single arg number params.
fn parse_go(mut words: Split<'_, char>) -> Message {
    let mut tc: Option<TimeControl> = None;
    let mut sc: Option<SearchControl> = None;

    while let Some(word) = words.next() {
        match word {
            "ponder" => tc = Some(TimeControl::Ponder),
            "infinite" => tc = Some(TimeControl::Infinite),
            "movetime" => {
                let movetime = pls!(u64::from_str(pls!(words.next())).ok());
                tc = Some(TimeControl::MoveTime(movetime));
            }

            "depth" => parse_intarg!(u8, sc, depth, pls!(words.next())),
            "nodes" => parse_intarg!(u64, sc, nodes, pls!(words.next())),
            "mate" => parse_intarg!(u8, sc, mate, pls!(words.next())),

            "wtime" => time_intarg!(u64, tc, white_time, pls!(words.next())),
            "btime" => time_intarg!(u64, tc, black_time, pls!(words.next())),
            "winc" => time_intarg!(u64, tc, white_increment, pls!(words.next())),
            "binc" => time_intarg!(u64, tc, black_increment, pls!(words.next())),
            "movestogo" => time_intarg!(u8, tc, moves_to_go, pls!(words.next())),

            "searchmoves" => {
                // variadic!
                let mut moves = Vec::new();
                while let Some(word) = words.next() {
                    moves.push(pls!(move_from_string(word.to_string())));
                }

                if sc.is_none() {
                    sc = Some(SearchControl::default());
                }

                if let Some(mut sc) = sc.as_mut() {
                    sc.search_moves = moves;
                }

                break; // maybe not needed since we slurped all the words.
            }
            _ => (),
        };
    }

    Message::Go(Go {
        time_control: tc,
        search_control: sc,
    })
}

/// Parse a uci message from a string.
pub fn parse(s: &str) -> Option<Message> {
    let mut words = s.split(' ');

    Some(match words.next()? {
        "ponderhit" => Message::PonderHit,
        "stop" => Message::Stop,
        "quit" => Message::Quit,
        "isready" => Message::IsReady,
        "ucinewgame" => Message::UciNewGame,
        "go" => parse_go(words),
        "uci" => Message::Uci,
        "debug" => match words.next()? {
            "true" => Message::Debug(true),
            "false" => Message::Debug(false),
            _ => return None,
        },
        "position" => parse_position(words)?,
        _ => return None,
    })
}

#[cfg(test)]
mod tests {
    use super::*;
    use chess::Square;

    #[test]
    fn test_simple() {
        // simple no-arg commands.
        assert_eq!(parse("ponderhit"), Some(Message::PonderHit));
        assert_eq!(parse("stop"), Some(Message::Stop));
        assert_eq!(parse("quit"), Some(Message::Quit));
        assert_eq!(parse("ucinewgame"), Some(Message::UciNewGame));
        assert_eq!(parse("uci"), Some(Message::Uci));
        assert_eq!(parse("isready"), Some(Message::IsReady));
    }

    #[test]
    fn test_position() {
        assert_eq!(
            parse("position startpos"),
            Some(Message::Position(Position {
                startpos: true,
                fen: None,
                moves: Vec::new(),
            }))
        );
    }

    #[test]
    fn test_debug() {
        assert_eq!(parse("debug true"), Some(Message::Debug(true)));
        assert_eq!(parse("debug false"), Some(Message::Debug(false)));
        assert_eq!(parse("debug asdofa"), None);
    }

    #[test]
    fn test_go() {
        assert_eq!(
            parse("go ponder"),
            Some(Message::Go(Go {
                time_control: Some(TimeControl::Ponder),
                search_control: None,
            }))
        );

        assert_eq!(
            parse("go infinite"),
            Some(Message::Go(Go {
                time_control: Some(TimeControl::Infinite),
                search_control: None,
            }))
        );

        assert_eq!(
            parse("go"),
            Some(Message::Go(Go {
                time_control: None,
                search_control: None,
            }))
        );

        assert_eq!(
            parse("go owo whats this?"),
            Some(Message::Go(Go {
                time_control: None,
                search_control: None,
            }))
        );

        assert_eq!(
            parse("go depth 2 nodes 1300"),
            Some(Message::Go(Go {
                time_control: None,

                search_control: Some(SearchControl {
                    depth: Some(2),
                    nodes: Some(1300),
                    mate: None,
                    search_moves: Vec::new(),
                })
            }))
        );

        assert_eq!(
            parse("go mate 3 searchmoves e2e4 e7e5"),
            Some(Message::Go(Go {
                time_control: None,

                search_control: Some(SearchControl {
                    mate: Some(3),
                    search_moves: vec![
                        ChessMove::new(Square::E2, Square::E4, None),
                        ChessMove::new(Square::E7, Square::E5, None),
                    ],
                    depth: None,
                    nodes: None,
                })
            }))
        );

        assert_eq!(
            parse("go wtime 13000 btime 13000"),
            Some(Message::Go(Go {
                time_control: Some(TimeControl::TimeLeft(TimeLeft {
                    white_time: Some(13000),
                    black_time: Some(13000),
                    white_increment: None,
                    black_increment: None,
                    moves_to_go: None,
                })),

                search_control: None,
            }))
        );

        assert_eq!(
            parse("go wtime 10 btime 20 winc 1000 binc 2000"),
            Some(Message::Go(Go {
                time_control: Some(TimeControl::TimeLeft(TimeLeft {
                    white_time: Some(10),
                    black_time: Some(20),
                    white_increment: Some(1000),
                    black_increment: Some(2000),
                    moves_to_go: None,
                })),

                search_control: None,
            }))
        );

        assert_eq!(
            parse("go wtime 13000 btime 13000 winc 1000 binc 2000 movestogo 30"),
            Some(Message::Go(Go {
                time_control: Some(TimeControl::TimeLeft(TimeLeft {
                    white_time: Some(13000),
                    black_time: Some(13000),
                    white_increment: Some(1000),
                    black_increment: Some(2000),
                    moves_to_go: Some(30),
                })),

                search_control: None,
            }))
        );

        assert_eq!(
            parse("go movetime 1000"),
            Some(Message::Go(Go {
                time_control: Some(TimeControl::MoveTime(1000)),
                search_control: None,
            }))
        );
    }
}
